var levels = {
    // Level data
    data: [
        {
            // Level 1
            foreground: 'desert-foreground',
            background: 'clouds-background',
            entities: [
                { type: "ground", name: "dirt", x: 500, y: 440, width: 1000, height: 20, isStatic: true },
                { type: "ground", name: "wood", x: 180, y: 390, width: 40, height: 80, isStatic: true },

                { type: "block", name: "wood", x: 520, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 520, y: 275, angle: 90, width: 100, height: 25 },
                { type: "villain", name: "burger", x: 520, y: 200, calories: 590 },

                { type: "block", name: "wood", x: 620, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 620, y: 275, angle: 90, width: 100, height: 25 },
                { type: "villain", name: "fries", x: 620, y: 200, calories: 420 },

                { type: "hero", name: "orange", x: 90, y: 410 },
                { type: "hero", name: "apple", x: 150, y: 410 }
            ]
        },
        {
            // Level 2
            foreground: 'desert-foreground',
            background: 'clouds-background',
            entities: [
                { type: "ground", name: "dirt", x: 500, y: 440, width: 1000, height: 20, isStatic: true },
                { type: "ground", name: "wood", x: 180, y: 390, width: 40, height: 80, isStatic: true },
                { type: "block", name: "wood", x: 820, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 720, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 620, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 670, y: 310, width: 100, height: 25 },
                { type: "block", name: "glass", x: 770, y: 310, width: 100, height: 25 },

                { type: "block", name: "glass", x: 670, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 770, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 720, y: 180, width: 100, height: 25 },

                { type: "villain", name: "burger", x: 715, y: 160, calories: 590 },
                { type: "villain", name: "fries", x: 670, y: 400, calories: 420 },
                { type: "villain", name: "sodacan", x: 765, y: 395, calories: 150 },

                { type: "hero", name: "strawberry", x: 40, y: 420 },
                { type: "hero", name: "orange", x: 90, y: 410 },
                { type: "hero", name: "apple", x: 150, y: 410 }
            ]
			
        },
		{
			// Level 3
            foreground: 'desert-foreground',
            background: 'clouds-background',
            entities: [
                { type: "ground", name: "dirt", x: 500, y: 440, width: 1000, height: 20, isStatic: true },
                { type: "ground", name: "wood", x: 180, y: 390, width: 40, height: 80, isStatic: true },
				{ type: "block", name: "wood", x: 920, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 820, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 720, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 620, y: 375, angle: 90, width: 100, height: 25 },
				{ type: "block", name: "wood", x: 594, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 670, y: 310, width: 100, height: 25 },
                { type: "block", name: "glass", x: 770, y: 310, width: 100, height: 25 },
				{ type: "block", name: "glass", x: 870, y: 310, width: 100, height: 25 },

				{ type: "block", name: "glass", x: 644, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 670, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "glass", x: 770, y: 248, angle: 90, width: 100, height: 25 },
				{ type: "block", name: "glass", x: 870, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 720, y: 180, width: 100, height: 25 },
				{ type: "block", name: "wood", x: 820, y: 180, width: 100, height: 25 },

                { type: "villain", name: "burger", x: 715, y: 160, calories: 590 },
				{ type: "villain", name: "burger", x: 815, y: 160, calories: 590 },
                { type: "villain", name: "fries", x: 670, y: 400, calories: 420 },
                { type: "villain", name: "sodacan", x: 765, y: 395, calories: 150 },
				{ type: "villain", name: "fries", x: 870, y: 400, calories: 420 },
				
                { type: "hero", name: "strawberry", x: 40, y: 420 },
                { type: "hero", name: "strawberry", x: 90, y: 410 },
                { type: "hero", name: "strawberry", x: 150, y: 410 }
            ]
        },
        {
			// Level 4
            foreground: 'desert-foreground',
            background: 'clouds-background',
            entities: [
                { type: "ground", name: "dirt", x: 500, y: 440, width: 1000, height: 20, isStatic: true },
                { type: "ground", name: "wood", x: 180, y: 390, width: 40, height: 80, isStatic: true },
				{ type: "block", name: "wood", x: 920, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 820, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 720, y: 375, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 620, y: 375, angle: 90, width: 100, height: 25 },				
                { type: "block", name: "wood", x: 670, y: 310, width: 100, height: 25 },
                { type: "block", name: "wood", x: 770, y: 310, width: 100, height: 25 },
				{ type: "block", name: "wood", x: 870, y: 310, width: 100, height: 25 },
				
                { type: "block", name: "wood", x: 670, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 770, y: 248, angle: 90, width: 100, height: 25 },
				{ type: "block", name: "wood", x: 870, y: 248, angle: 90, width: 100, height: 25 },
                { type: "block", name: "wood", x: 720, y: 180, width: 100, height: 25 },
				{ type: "block", name: "wood", x: 820, y: 180, width: 100, height: 25 },

                // { type: "villain", name: "burger", x: 715, y: 160, calories: 590 },
				// { type: "villain", name: "burger", x: 815, y: 160, calories: 590 },
                { type: "villain", name: "fries", x: 670, y: 400, calories: 420 },
                { type: "villain", name: "sodacan", x: 765, y: 395, calories: 150 },
				{ type: "villain", name: "fries", x: 870, y: 400, calories: 420 },
				
                { type: "hero", name: "watermelon", x: 30, y: 420 },
                { type: "hero", name: "watermelon-slice", x: 90, y: 420 },
                { type: "hero", name: "watermelon-slice", x: 170, y: 420 }
            ]
        },
    ],

    // Init level select
    init: function () {
        var html = "";

        for (var i = 0; i < levels.data.length; i++) {
            var level = levels.data[i];
            html += '<input type="button" value="' + (i + 1) + '">';
        }

        $('#level_select_screen').html(html);

        $('#level_select_screen input').click(function () {
            levels.load(this.value - 1);
            $('#level_select_screen').hide();
        });
    },

    // Load level data and images
    load: function (number) {
        // Init Box2D world
        box2d.init();

        // create new currentLevel object
        game.currentLevel = { number: number, hero: [] };

        game.score = 0;
        $('#score').html('Score: ' + game.score);
        var level = levels.data[number];

        // Load background and foreground, slingshot
        game.currentLevel.backgroundImage = loader.loadImage("images/backgrounds/" + level.background + ".png");
        game.currentLevel.foregroundImage = loader.loadImage("images/backgrounds/" + level.foreground + ".png");
        game.slingshotImage = loader.loadImage("images/slingshot.png");
        game.slingshotFrontImage = loader.loadImage("images/slingshot-front.png");

        // Iterate through the level entities data and load them
        for (var i = level.entities.length - 1; i >= 0; i--) {
            var entity = level.entities[i];
            entities.create(entity);
        }

        if (loader.loaded) {
            game.start();
        } else {
            loader.onload = game.start;
        }
    }
};