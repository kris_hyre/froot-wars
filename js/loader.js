// Sound and Image asset loader
var loader = {
    loaded: true,
    loadedCount: 0,
    totalCount: 0,


    init: function () {
        // Test browser for mp3 or ogg support
        var mp3Support, oggSupport;
        var audio = document.createElement('audio');
        if (audio.canPlayType) {
            mp3Support = "" !== audio.canPlayType('audio/mpeg');
            oggSupport = "" !== audio.canPlayType('audio/ogg; codecs = "vorbis"');
        } else {
            mp3Support = false;
            oggSupport = false;
        }
        // Set sound file extension to supported type or undefined
        loader.soundFileExtn = oggSupport ? ".ogg" : mp3Support ? ".mp3" : undefiend;
    },

    loadImage: function (url) {
        this.totalCount++;
        this.loaded = false;
        $('#loading_screen').show();
        var image = new Image();
        image.src = url;
        image.onload = loader.itemLoaded;
        return image;
    },

    soundFileExtn: ".ogg",

    loadSound: function (url) {
        this.totalCount++;
        this.loaded = false;
        $('#loading_screen').show();
        var audio = new Audio();
        audio.src = url + loader.soundFileExtn; // append the supported audio file type
        audio.addEventListener("canplaythrough", loader.itemLoaded, false);
        return audio;
    },

    itemLoaded: function () {
        loader.loadedCount++;
        $('#loading_message').html('Loaded ' + loader.loadedCount + ' of ' + loader.totalCount);
        if (loader.loadedCount === loader.totalCount) {
            // loading complete
            loader.loaded = true;
            $('#loading_screen').hide();
            if (loader.onload) {
                loader.onload();
                loader.onload = undefined;
            }
        }
    }
};