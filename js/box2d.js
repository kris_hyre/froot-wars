// Box2D variables
var b2Vec2 = Box2D.Common.Math.b2Vec2;
var b2BodyDef = Box2D.Dynamics.b2BodyDef;
var b2Body = Box2D.Dynamics.b2Body;
var b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
var b2Fixture = Box2D.Dynamics.b2Fixture;
var b2World = Box2D.Dynamics.b2World;
var b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
var b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
var b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

var box2d = {
    scale: 30,
    debugCanvas: undefined,

    init: function () {
        var gravity = new b2Vec2(0, 9.8); // gravity set to 9.8 m/s^2
        var allowSleep = true; // allow objects at rest to be excluded from calcs
        box2d.world = new b2World(gravity, allowSleep);

        // this.setupDebugDraw();

        var listener = new Box2D.Dynamics.b2ContactListener;
        listener.PostSolve = function (contact, impulse) {
            var body1 = contact.GetFixtureA().GetBody();
            var body2 = contact.GetFixtureB().GetBody();
            var entity1 = body1.GetUserData();
            var entity2 = body2.GetUserData();

            var impulseAlongNormal = Math.abs(impulse.normalImpulses[0]);
            // filter tiny impulses
            if (impulseAlongNormal > 5) {
                if (entity1.health) {
                    entity1.health -= impulseAlongNormal;
                }

                if (entity2.health) {
                    entity2.health -= impulseAlongNormal;
                }
            }
			/* TODO: Debugging the bounce sounds are currently breaking loading the next level
			 *       or starting the next after the initial level is cleared
			 *		 temp removing the sounds
			 */
			// play sounds
			if (entity1.bounceSound){
				// TODO: debugging entity1.bounceSound.play();
			}
			if (entity2.bounceSound){
				// TODO: debugging entity2.bounceSound.play();
			}
        };
        box2d.world.SetContactListener(listener);
    },

    setupDebugDraw: function () {
        // Dynamically create a canvas for the debug drawing
        if (!box2d.debugCanvas) {
            var canvas = document.createElement("debug_canvas");

            canvas.width = 1024;
            canvas.height = 480;
            document.body.appendChild(canvas);
            canvas.style.top = "480px";
            canvas.style.position = "absolute";
            canvas.style.background = "white";
            box2d.debugCanvas = canvas;
        }

        // Set up debug draw
        var debugContext = box2d.debugCanvas.getContext("2d");
        var debugDraw = new b2DebugDraw();

        debugDraw.SetSprite(debugContext);
        debugDraw.SetDrawScale(box2d.scale);
        debugDraw.SetFillAlpha(0.3);
        debugDraw.SetLineThickness(1.0);
        debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
        box2d.world.SetDebugDraw(debugDraw);
    },

    createRectangle: function (entity, definition) {
        var bodyDef = new b2BodyDef;
        if (entity.isStatic) {
            bodyDef.type = b2Body.b2_staticBody;
        } else {
            bodyDef.type = b2Body.b2_dynamicBody;
        }
        bodyDef.position.x = entity.x / box2d.scale;
        bodyDef.position.y = entity.y / box2d.scale;
        if (entity.angle) {
            bodyDef.angle = Math.PI * entity.angle / 180;
        }

        var fixtureDef = new b2FixtureDef;
        fixtureDef.density = definition.density;
        fixtureDef.friction = definition.friction;
        fixtureDef.restitution = definition.restitution;

        fixtureDef.shape = new b2PolygonShape;
        fixtureDef.shape.SetAsBox(entity.width / 2 / box2d.scale, entity.height / 2 / box2d.scale);

        var body = box2d.world.CreateBody(bodyDef);
        body.SetUserData(entity);

        var fixture = body.CreateFixture(fixtureDef);
        return body;
    },

    createCircle: function (entity, definition) {
        var bodyDef = new b2BodyDef;
        if (entity.isStatic) {
            bodyDef.type = b2Body.b2_staticBody;
        } else {
            bodyDef.type = b2Body.b2_dynamicBody;
        }
        bodyDef.position.x = entity.x / box2d.scale;
        bodyDef.position.y = entity.y / box2d.scale;
        if (entity.angle) {
            bodyDef.angle = Math.PI * entity.angle / 180;
        }

        var fixtureDef = new b2FixtureDef;
        fixtureDef.density = definition.density;
        fixtureDef.friction = definition.friction;
        fixtureDef.restitution = definition.restitution;

        fixtureDef.shape = new b2CircleShape(entity.radius / box2d.scale);

        var body = box2d.world.CreateBody(bodyDef);
        body.SetUserData(entity);

        var fixture = body.CreateFixture(fixtureDef);
        return body;
    },

    createTriangle: function (entity, definition) {
        // new function to add triangles to the game
        var bodyDef = new b2BodyDef;
        if (entity.isStatic) {
            bodyDef.type = b2Body.b2_staticBody;
        } else {
            bodyDef.type = b2Body.b2_dynamicBody;
        }
        bodyDef.position.x = entity.x / box2d.scale;
        bodyDef.position.y = entity.y / box2d.scale;
        if (entity.angle) {
            bodyDef.angle = Math.PI * entity.angle / 180;
        }

        var fixtureDef = new b2FixtureDef;
        fixtureDef.density = definition.density;
        fixtureDef.friction = definition.friction;
        fixtureDef.restitution = definition.restitution;

        fixtureDef.shape = new b2PolygonShape;
        fixtureDef.shape.SetAsArray([
            // TODO: Gotta do the math to place the vectors for an equilateral triangle 
            // These are just using some spitballed values at the moment for testing purposes
            new b2Vec2(entity.radius * 0.866 / box2d.scale, entity.radius * 0.5 / box2d.scale), // old value 0.866
            new b2Vec2(entity.radius * -0.866 / box2d.scale, entity.radius * 0.5 / box2d.scale), // old value -0.866
            new b2Vec2(0, entity.radius * -1 / box2d.scale),
        ]);

        var body = box2d.world.CreateBody(bodyDef);
        body.SetUserData(entity);

        var fixture = body.CreateFixture(fixtureDef);
        return body;
    },

    step: function (timeStep) {
        // velocity iterations = 8
        // position iterations = 3
        box2d.world.Step(timeStep, 8, 3);
    }
};
