var mouse = {
    x: 0,
    y: 0,
    down: false,
    dragging: false,

    init: function () {
        var canvas = document.getElementById("game_canvas");

        canvas.addEventListener("mousemove", mouse.mouse_move_handler, false);
        canvas.addEventListener("mousedown", mouse.mouse_down_handler, false);
        canvas.addEventListener("mouseup", mouse.mouse_up_handler, false);
        canvas.addEventListener("mouseout", mouse.mouse_up_handler, false);
    },

    mouse_move_handler: function (ev) {
        var offset = game.canvas.getBoundingClientRect();

        mouse.x = ev.clientX - offset.left;
        mouse.y = ev.clientY - offset.top;

        if (mouse.down) {
            mouse.dragging = true;
        }

        ev.preventDefault();
    },

    mouse_down_handler: function (ev) {
        mouse.down = true;

        ev.preventDefault();
    },

    mouse_up_handler: function (ev) {
        mouse.down = false;
        mouse.dragging = false;

        ev.preventDefault();
    }
};