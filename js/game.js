/* "Froot Wars" was created and expanded on by Kris Hyre using the tutorial information 
 * and resources from Aditya Ravi Shankar's "Pro HTML 5 Games"
 * https://github.com/apress/pro-html5-games-17.
 * 
 * JQuery is used under the MIT License https://jquery.org/license/
 *
 * Box2DWeb https://github.com/hecht-software/box2dweb was ported from Box2D. 
 * Box2D was developed by Erin Catto and released under zlib license http://box2d.org/about/
 */

// Box2D variables
var b2Vec2 = Box2D.Common.Math.b2Vec2;
var b2BodyDef = Box2D.Dynamics.b2BodyDef;
var b2Body = Box2D.Dynamics.b2Body;
var b2FixtureDef = Box2D.Dynamics.b2FixtureDef;
var b2Fixture = Box2D.Dynamics.b2Fixture;
var b2World = Box2D.Dynamics.b2World;
var b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
var b2CircleShape = Box2D.Collision.Shapes.b2CircleShape;
var b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

/* requestAnimation polyfill function to handle different browser
 * implementations and otherwise handle window.requestAnimation 
 * and window.cancelAnimation
 */ 
(function(){
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x){
		window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x] + 'CanceAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
	}
	
	if (!window.requestAnimationFrame){
		window.requestAnimationFrame = function(callback, element){
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id  = windows.setTimeout(function(){ callback(currTime + timeToCall); }, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}
	
	if (!window.cancelAnimationFrame){
		window.cancelAnimationFrame = function(id){
			clearTimeout(id);
		};
	}
	
}());

/* The main game loop */
var game = {

	highScore: 0,
	mode: "intro",
		
	// game score
	score: 0,
	
	// panning variables
	maxSpeed: 3,
	minOffset: 0,
    maxOffset: 300,
	
	// Slingshot coords
	slingshotX: 140,
	slingshotY: 280,
	
	init: function(){
		// Initialize game data
		levels.init();
		loader.init();
		mouse.init();
		
		// checks to see if existing high score cookie
		game.checkHighScore();
		$('#high_score').html('High Score: ' + game.highScore);

		// Load music and sound fx
		
		//"Kindergarten" by Gurdonark
		//http://ccmixter.org/files/gurdonark/26491 is licensed under a Creative Commons license
		game.backgroundMusic = loader.loadSound("audio/gurdonark-kindergarten");
		
		game.slingshotReleasedSound = loader.loadSound("audio/released");
		game.bounceSound = loader.loadSound("audio/bounce");
		
		game.breakSound = {
			"glass": loader.loadSound("audio/glassbreak"),
			"wood": loader.loadSound("audio/woodbreak")
		};
		
		// Display start screen
		$('.game_layer').hide();
		$('#game_start_screen').show();
		
		// set the canvas and context for the game
		game.canvas = $('#game_canvas')[0];
		game.context = game.canvas.getContext('2d');
	},
	
	showLevelScreen: function(){
		$('.game_layer').hide();
		$('#level_select_screen').show('slow');		
	},
	
	start: function(){
		$('.game_layer').hide();
		$('#game_canvas').show();
		$('#score_screen').show();
		
		// Check music option
		var musicSettingPaused = game.checkGameCookie("musicPaused");
		if (musicSettingPaused){ 
			game.stopBackgroundMusic();
			game.setBackgroundMusicButton();
		} else { 
			game.startBackgroundMusic();
			game.setBackgroundMusicButton();
		}
		
		
		// level menu restarts
		window.cancelAnimationFrame(game.animationFrame);
		game.lastUpdateTime = undefined;
		game.currentHero = undefined;
		
		game.mode = "intro";
		game.offsetLeft = 0;
		game.ended = false;
		game.animationFrame = window.requestAnimationFrame(game.animate, game.canvas);
	},

	// Pan the screen to center on newCenter
	panTo: function(newCenter){		
		if (Math.abs(newCenter - game.offsetLeft - game.canvas.width / 4) > 0 && game.offsetLeft <= game.maxOffset && game.offsetLeft >= game.minOffset){ // TODO: phew, this is wordy, work on this
			var deltaX = Math.round((newCenter - game.offsetLeft - game.canvas.width / 4) / 2);
			if (deltaX && Math.abs(deltaX) > game.maxSpeed){
				deltaX = game.maxSpeed * Math.abs(deltaX) / deltaX;
			}
			game.offsetLeft += deltaX;
		} else {
			return true;
		}
		if (game.offsetLeft < game.minOffset){
			game.offsetLeft = game.minOffset;
			return true;
		} else if (game.offsetLeft > game.maxOffset){
			game.offsetLeft = game.maxOffset;
			return true;
		}
		return false;
    },

	// Counts the heroes and villain entities box2d bodies that still exist
    countHeroesAndVillains: function () {
        game.heroes = [];
        game.villains = [];
        for (var body = box2d.world.GetBodyList(); body; body = body.GetNext()) {
            var entity = body.GetUserData();
            if (entity) {
                if (entity.type === "hero") {
                    game.heroes.push(body);
                } else if (entity.type === "villain") {
                    game.villains.push(body);
                }
            }
        }
    },
	
	handlePanning: function(){
		switch(game.mode){
		
			case "intro":
				if (game.panTo(700)){
					game.mode = "load-next-hero";
				}
				break;
				
			case "wait-for-firing":
				if (mouse.dragging) {
					if (game.mouseOnCurrentHero()) {
						game.mode = "firing";
					} else {
						game.panTo(mouse.x + game.offsetLeft);
					}
				} else {
					game.panTo(game.slingshotX);
				}
				break;
				
			case "firing":
				if (mouse.down) {
					// drag the fruit around
					game.panTo(game.slingshotX);
					game.currentHero.SetPosition({ x: (mouse.x + game.offsetLeft) / box2d.scale, y: mouse.y / box2d.scale });
				} else {
					// apply impulse and shoot da fruit
					game.mode = "fired";
					var impulseScaleFactor = 0.75;
					var slingshotCenterX = game.slingshotX + 35;
					var slingshotCenterY = game.slingshotY + 25;
					var impulse = new b2Vec2((slingshotCenterX - mouse.x - game.offsetLeft) * impulseScaleFactor, (slingshotCenterY - mouse.y) * impulseScaleFactor);
					game.currentHero.ApplyImpulse(impulse, game.currentHero.GetWorldCenter());
				}
				break;
			
			case "fired":				
				var heroX = game.currentHero.GetPosition().x * box2d.scale; // pan to the hero location
				game.panTo(heroX);				
				game.checkHeroState(heroX); // wait until rest or out of bounds
				break;
				
			case "load-next-hero":
				game.countHeroesAndVillains();

				// if no villains alive, end level with success
				if (game.villains.length === 0) {
					game.mode = "level-success";
					return;
				}

				// if there are no heroes left, end game mode in failure
				if (game.heroes.length === 0) {
					game.mode = "level-failure";
					return;
				}

				// else, load a hero and play on
				if (!game.currentHero) {
					game.currentHero = game.heroes[game.heroes.length - 1];
					game.currentHero.SetPosition({ x: 180 / box2d.scale, y: 200 / box2d.scale });
					game.currentHero.SetLinearVelocity({ x: 0, y: 0 });
					game.currentHero.SetAngularVelocity(0);
					game.currentHero.SetAwake(true);
				} else {
					// Wait for hero to stop bouncing and fall asleep and then switch to wait-for-firing
					game.panTo(game.slingshotX);
					if (!game.currentHero.IsAwake()) {
						game.mode = "wait-for-firing";
					}
				}
				break;
				
			case "level-success":
			case "level-failure":
				if (game.panTo(0)) {
					game.ended = true;
					game.showEndingScreen();
				}
				break;
			
			default:
				console.log("Unhandled game.mode");
				break;
		}
	},
	
	animate: function(){
		// Animate background
        game.handlePanning();

        // Character animation
        var currentTime = new Date().getTime();
        var timeStep;
        if (game.lastUpdateTime) {
            timeStep = (currentTime - game.lastUpdateTime) / 1000;
            box2d.step(timeStep);
        }

        game.lastUpdateTime = currentTime;
		
		// Parallax scroll background images
		game.context.drawImage(game.currentLevel.backgroundImage, game.offsetLeft / 4, 0, 640, 480, 0, 0, 640, 480);
		game.context.drawImage(game.currentLevel.foregroundImage, game.offsetLeft, 0, 640, 480, 0, 0, 640, 480);
		
		// draw slingshot
		game.context.drawImage(game.slingshotImage, game.slingshotX - game.offsetLeft, game.slingshotY);
		game.drawAllBodies();
		if(game.mode === "firing"){
			game.drawSlingshotBand();
		}
		game.context.drawImage(game.slingshotFrontImage, game.slingshotX - game.offsetLeft, game.slingshotY);
		
		if (!game.ended){
			game.animationFrame = window.requestAnimationFrame(game.animate, game.canvas);
		}
	},

	checkHeroState: function(heroX){
		var heroX = heroX;
		if (!game.currentHero.IsAwake() || heroX < 0 || heroX > game.currentLevel.foregroundImage.width) { // OOB or stopped, delete the old hero
			box2d.world.DestroyBody(game.currentHero);
			game.currentHero = undefined;
			game.mode = "load-next-hero"; // load next hero
		}
	},
	
	drawAllBodies: function(){		
		// Iterate through all bodies
		for (var body = box2d.world.GetBodyList(); body; body = body.GetNext()){
			var entity = body.GetUserData();
			
            if (entity) {
                var entityX = body.GetPosition().x * box2d.scale;
                if (entityX < 0 || entityX > game.currentLevel.foregroundImage.width || entity.health && entity.health <= 0) {
                    box2d.world.DestroyBody(body);
                    if (entity.type === "villain") {
                        game.score += entity.calories;
						if (game.highScore <= game.score){
							game.highScore = game.score;
						}	
                        $('#score').html('Score: ' + game.score);
						$('#high_score').html('High Score: ' + game.highScore);
                    } 
					if (entity.breakSound){
						// TODO: debugging entity.breakSound.play();
					}
                } else {
                    entities.draw(entity, body.GetPosition(), body.GetAngle());
                }
				
			}
		}
    },

    mouseOnCurrentHero: function () {
        if (!game.currentHero) {
            return false;
        }
        /* Calculate the distance from the mouse location to the hero center 
         * If less than the hero radius, it is moused over
         */
        var position = game.currentHero.GetPosition();
        var distanceSquared = Math.pow(position.x * box2d.scale - mouse.x - game.offsetLeft, 2) + Math.pow(position.y * box2d.scale - mouse.y, 2);
        var radiusSquared = Math.pow(game.currentHero.GetUserData().radius, 2);
        return distanceSquared <= radiusSquared;
    },

    showEndingScreen: function () {
		console.log(game.currentLevel.number + " " + (levels.data.length - 1));
		console.log(game.mode);
		game.stopBackgroundMusic();
		/*
		if (game.score >= game.highScore) {
			game.setGameCookie("highscore", game.highScore, 365);
		}*/
        if (game.mode == "level-success") {
            if (game.currentLevel.number < levels.data.length - 1) {
                $('#ending_message').html('Level Complete! Well Done!');
                $('#play_next_level').show();
            } else {
				$('#ending_message').html('All Levels Complete! Well Done!');
				$('#play_next_level').hide();             
			}	 
        }
		if (game.mode == "level-failure") {
			if (game.currentLevel.number < levels.data.length - 1) {
				$('#play_next_level').show();
			} else {
				$('#play_next_level').hide();
			}
		}
        $('#ending_screen').show();
    },

	drawSlingshotBand: function(){
		game.context.strokeStyle = "rgb(68, 31, 11)";
		game.context.lineWidth = 6;
		
		// uses the angle and radius of the dragged hero's location in drawing the band
		var radius = game.currentHero.GetUserData().radius;
		var heroX = game.currentHero.GetPosition().x * box2d.scale;
		var heroY = game.currentHero.GetPosition().y * box2d.scale;
		var angle = Math.atan2(game.slingshotY + 25 - heroY, game.slingshotX + 50 - heroX);
		
		var heroFarEdgeX = heroX - radius * Math.cos(angle);
		var heroFarEdgeY = heroY - radius * Math.sin(angle);
		
		// Draw a band from the back of the slingshot to the centre of the hero
		game.context.beginPath();
		game.context.moveTo(game.slingshotX + 50 - game.offsetLeft, game.slingshotY + 25);
		
		game.context.lineTo(heroX - game.offsetLeft, heroY);
		game.context.stroke();
		
		entities.draw(game.currentHero.GetUserData(), game.currentHero.GetPosition(), game.currentHero.GetAngle());
		
		game.context.beginPath();
		game.context.moveTo(heroFarEdgeX - game.offsetLeft, heroFarEdgeY);
		
		game.context.lineTo(game.slingshotX - game.offsetLeft + 10, game.slingshotY + 30);
		game.context.stroke();
	},
	
	restartLevel: function(){
		window.cancelAnimationFrame(game.animationFrame);
		game.lastUpdateTime = undefined;
		game.currentHero = undefined;
		levels.load(game.currentLevel.number);
	},
	
	startNextLevel: function(){
		window.cancelAnimationFrame(game.animationFrame);
		game.lastUpdateTime = undefined;
		game.currentHero = undefined;
		levels.load(game.currentLevel.number + 1);
	},
	
	startBackgroundMusic: function(){
		game.backgroundMusic.play();
		game.setBackgroundMusicButton();
	},
	
	stopBackgroundMusic: function(){
		game.backgroundMusic.pause();
		game.backgroundMusic.currentTime = 0;
		game.setBackgroundMusicButton();
	},
	
	toggleBackgroundMusic: function(){
		if (game.backgroundMusic.paused){
			game.backgroundMusic.play();
			game.setGameCookie("musicPaused", false, 365);

		} else {
			game.backgroundMusic.pause();
			game.backgroundMusic.currentTime = 0;
			game.setGameCookie("musicPaused", true, 365);
		}
		game.setBackgroundMusicButton();
	},
	
	setBackgroundMusicButton: function() {
        var toggleImage = document.getElementById("toggle_music");

        if (game.backgroundMusic.paused) {
            toggleImage.src = "images/icons/nosound.png";
        } else {
            toggleImage.src = "images/icons/sound.png";
        }
    },
		
	checkHighScore: function(){
		var retrievedScore = game.checkGameCookie("highscore");
		if (retrievedScore){
			game.highScore = retrievedScore;
		} else {
			game.highScore = 0;
		}
	},
	
	// Allows for more values in the document cookie
	checkGameCookie: function(value){
		var searchValue = value + "=";
		var searchArray = document.cookie.split(";"); // break up the cookie into an array by ;
		for (var i = 0; i < searchArray.length; i++){
			var cookie = searchArray[i];
			while (cookie.charAt(0) == " "){
				cookie = cookie.substring(1, cookie.length); // remove any leading spaces
			}
			if (cookie.indexOf(searchValue) == 0){
				return cookie.substring(searchValue.length, cookie.length); // return value for the key
			} 		
		}
		return null; // search value key not found in the document cookie
	},
	
	/* Currently holds values for the highscore and 
	 * if the background music should be on or off
	 * Expires in 1 year
	 */
	setGameCookie: function(key, value, time){
		var date = new Date();
		date.setTime(date.getTime() + (time * 24 * 60 * 60 * 1000));
		var expireIn = "; expires=" + date.toGMTString();
		document.cookie = key + "=" + value + expireIn + "; path=/";
		console.log(document.cookie);
	},	
};

window.addEventListener("load", function () {
    game.init();
});